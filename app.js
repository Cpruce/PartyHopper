var express = require('express');
var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy
var graph = require('fbgraph');
var path = require('path');
var mongoose = require('mongoose');

var accessTokens = [];
var wHaps = [];
var cHaps = [];
var mHaps = [];
var count = 0;

mongoose.connect('mongodb://localhost/partyhopper');
var user_schema = mongoose.Schema({
    _id: Number,
    first_name: String,
    last_name : String,
    username: String,
    last_updated: Date,
    friends: [{
        id:String,
        name:String,
    }],
    photoUrl: String,

});

var hap_schema = mongoose.Schema({
    _id: Number,
    name: String,
    location : String,
    timezone: String,
    start_time: Date,
    end_time: Date,
    date: Date,
    details: String,
    thumbsUp: Number,
    thumbsDown: Number,
    people: [{
        id:Number,
        name:String,
    }],
});

var User = mongoose.model('User', user_schema);

var Hap = mongoose.model('Hap', hap_schema);

// Retrieve a user from storage
function getUser(user_id, done) {
    User.findOne({_id:user_id}, function(err, user){
        done(err, user);
    });
}

function getHap(hap_id, done){
    Hap.findOne({_id:hap_id}, function(err, hap){
        done(err, hap);
    });
}

function checkKeywords(hapLoc){
    var kwords = ["Pomona College", "Pomona", "Pitzer College", "Harvey Mudd", "Claremont", "Claremont McKenna", "Scripps"];
    for (var i = kwords.length - 1; i >= 0; i--) {
        if ((hapLoc).indexOf(kwords[i]) !== -1){
            return true;
        };
    };
    return false;
}


// Add a user to storage
function addUser(profile) {
    //var s = "https://graph.facebook.com/" + profile.id + "/picture";
    
    var user = new User({
        _id: profile.id,
        first_name: profile.name.givenName,
        last_name: profile.name.familyName,
        friends:[],
    });

    user.save();
    return user;
}

// Add a hap to storage
function addHap(evData) {
    var hap = new Hap({
        _id: evData.id,
        name: evData.name,
        location : evData.location,
        start_time: evData.start_time,
        end_time: evData.end_time,
        timezone: evData.timezone,
        thumbsUp: 0,
        thumbsDown: 0,
        people:[],
    });

    hap.save();
    return hap;
}

function thumbsUp(hap){
    hap.thumbsUp++;
}

function thumbsDown(hap){
    hap.thumbsDown++;
}

function getUsersAtHap(hap){
    return Hap.findOne({_id: hap._id}).people;
}

function checkIn(user_id, hap_id){
    Hap.findOne({_id:hap_id}, function(err, hap){
        //done(err, user);
        User.findOne({_id:user_id}, function(err, user) {
                        //console.log("user: "+user);
                        hap.people.push({id: user._id, name: user.name});
                        console.log("pushed: "+hap);
                     });
        //console.log("user: "+user);
        //hap.people.push(user_id);
        //console.log("pushed: "+hap);
        
    }); //people.push(user_id);
}

// Add a photo to the user's profile
function addPhoto(hap, photo) {
    Hap.update({_id:hap.id}, 
        {$push:{ photos:photo }},
        { $upsert: true},
        function(err, data){
            console.log(err);
        });
}

// Facebook app information
const FB_ID = '228372603994396';
const FB_APP_SECRET = '2c1f90d06c463e11f0ddfe4353c96a73';
const FB_CALLBACK_URL = 'http://localhost:3000/auth/facebook/callback';

// Necessary for saving users across sessions
passport.serializeUser(function(user, done) {
    done(null, user._id);
});

passport.deserializeUser(function(id, done) {
    getUser(id, function(err, user) {
        done(err, user);
    });
});

function getUserEvents(user, accessToken) {
    graph.setAccessToken(accessToken);
    count = 0;
    graph.get(user.id + '/events', 
        function(err, res){
            
            for (var i = 0; i < res.data.length; i++){
                if(!(res.data[i].name == "") && count < 5){
                    addHap(res.data[i]);
                    count++;
                }

            }

        }

    );

}

function getNearbyEvents(user, accessToken) {
    graph.setAccessToken(accessToken);
    var loc = user.location;
    count = 0;
    var kwords = ["Pomona College", "Pomona", "Pitzer College", "Harvey Mudd", "Claremont", "Claremont McKenna", "Scripps"];
    for (var i = kwords.length - 1; i >= 0; i--) {
        graph.get("search?q=" + kwords[i] + "&type=event", 
        function(err, res){
            
            for (var i = 0; i < res.data.length; i++){
                if(!(res.data[i].name == "")&& count < 5){
                    addHap(res.data[i]);
                    count++;
                }
            }

        }

        );
    };
}


passport.use(new FacebookStrategy({
    clientID: FB_ID,
    clientSecret: FB_APP_SECRET,
    callbackURL: FB_CALLBACK_URL,
    },
    function(accessToken, refreshToken, profile, done) {
        getUser(profile.id, function(err, user) {
            console.log(user);
            if (!user) {

                user = addUser(profile);
            }
            //updateFriends(user, accessToken);
            accessTokens[user._id] = accessToken;
            return done(null, user);
        });
    }
));

var app = express();
app.configure(function() {
    // Use ejs as the view engine
    app.set('view engine', 'ejs');

    // Set up passport magic
    app.use(express.cookieParser());
    app.use(express.json());
    app.use(express.urlencoded());
    app.use(express.session({ 'secret': 'whisper' }));
    app.use(passport.initialize());
    app.use(passport.session());

    // Indicate directory of static files
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(app.router);

});

// Authetication routes
app.get('/auth/facebook',
        passport.authenticate('facebook', { scope: ['user_friends', 'read_friendlists', 'user_status'] }),
        function(req, res) {
           // This is Facebook's job -- do nothing
           passport.use();
        });

app.get('/auth/facebook/callback',
        passport.authenticate('facebook', { successRedirect: '/', failureRedirect: '/' }));

app.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

// Site routes
app.get('/', function(req, res) {
    
    if (req.user) {
        
        getUserEvents(req.user, accessTokens[req.user._id]);
    
        getNearbyEvents(req.user, accessTokens[req.user._id]);
        
        res.render('index', {user: req.user});
    } else {
        res.render('index');
    }
});

app.get('/checkin/:user_id,:hap_id,:time', function(req, res) {
    var user_id = req.params.user_id;
    var hap_id = req.params.hap_id;
    var time = req.params.time;
    if (req.user) {
        checkIn(user_id, hap_id);
        if (time == 'week') {
            res.render('parties', {user: req.user, haps:wHaps, time:'week'});
        } else if (time == 'month') {
            res.render('parties', {user: req.user, haps:mHaps, time:'month'});
        } else {
            res.render('parties', {user: req.user, haps:cHaps, time:'now'});
        }
        //res.render('parties', {user: req.user, haps:haps});
    } else {
        res.render('index');
    }
});

app.get('/thumbsUp', function(req, res) {
    
    if (req.hap) {
        
        Hap.find({n: req.hap}, function(err, happen) {
            happen.thumbsUp++;
            res.render('index', {tU: happen.thumbsUp});    
            });
    } else {
        res.render('index');
    }
});

app.get('/thumbsDown', function(req, res) {
    
    if (req.hap) {
        
        Hap.find({n: req.hap}, function(err, happen) {
            happen.thumbsDown++;
            res.render('index', {tU: happen.thumbsDown});    
            });
    } else {
        res.render('index');
    }
});

app.get('/now', function(req, res) {
    if (req.user) {

        var today = new Date();
        var dd = today.getDate();
        var month = today.getUTCMonth();
        var year = today.getUTCFullYear();


        Hap.find({}, function(err, hs) {
            for (var i = hs.length - 1; i >= 0; i--) {
                var hD = hs[i].start_time.getDate();
                var hM = hs[i].start_time.getUTCMonth();
                var hY = hs[i].start_time.getUTCFullYear();
                if(year == hY && month == hM && dd == hD){ 
                    cHaps.push(hs[i]);
                }
            }
            
            res.render('parties', {user: req.user, haps: cHaps, time:'now'});
        })
        
    } else {
        res.render('index');
    }
});

app.get('/week', function(req, res) {
    if (req.user) {
        var today = new Date();
        var dd = today.getDate();
        var month = today.getUTCMonth();
        var year = today.getUTCFullYear();


        Hap.find({}, function(err, hs) {
            for (var i = hs.length - 1; i >= 0; i--) {
                var hD = hs[i].start_time.getDate();
                var hM = hs[i].start_time.getUTCMonth();
                var hY = hs[i].start_time.getUTCFullYear();
                if(year == hY && month == hM && dd <= hD && dd + 7 >= hD) { 
                    wHaps.push(hs[i]);
                }
            }
            
            res.render('parties', {user: req.user, haps: wHaps, time:'week'});
        })

    } else {
        res.render('index');
    }
});

app.get('/month', function(req, res) {
    if (req.user) {
        var today = new Date();
        var dd = today.getDate();
        var month = today.getUTCMonth();
        var year = today.getUTCFullYear();


        Hap.find({}, function(err, hs) {
            for (var i = hs.length - 1; i >= 0; i--) {
                var hD = hs[i].start_time.getDate();
                var hM = hs[i].start_time.getUTCMonth();
                var hY = hs[i].start_time.getUTCFullYear();
                if((year == hY && month == hM && dd <= hD) || (year == hY && month + 1 == hM && dd >= hD)){ 
                    mHaps.push(hs[i]);
                }
            }
            
            res.render('parties', {user: req.user, haps: mHaps, time:'month'});

        })
    } else {
        res.render('index');
    }
});

app.get('/user/:id', function(req, res) {
    var id = parseInt(req.params.id);
    if (req.user) {
        if (req.user._id === id) {
            // Render user profile
            res.render('profile', { user: req.user, posts: req.user.posts });
        } else {
            getUser(req.params.id, function(err, friend) {
                if (friend) {
                    res.render('profile', { user: friend });
                } else {
                    res.status(404).send('Not found');
                }
            });
        }
    } else {
        res.redirect('/', 401);
    }
});

app.post('/user/:user_id/wallpost', function(req, res) {
    if (!req.user) {
        res.redirect('/', 401);
    } else {
        var post = {
            time: new Date(),
            sender_id: req.user._id,
            sender_name: req.user.first_name + ' ' + req.user.last_name,
            text: req.body['post']
        };
        addPost(req.user, post);
        res.redirect('/user/' + req.user._id);
    }
});

app.post('/user/:user_id/upload', function(req, res) {
    if (!req.user) {
        res.redirect('/', 401);
    } else {
        var photo = {
            url: req.body['url']
        };
        addPhoto(req.user, photo);
        res.redirect('/user/' + req.user._id);
    }
});

app.listen(3000);
